import React, { Component } from "react";
import { connect } from "react-redux";
import { OPEN_CART_MODAL } from "./redux/constant/shoeShopConstants";
import ListShoes from "./ListShoes";
import ShoesCart from "./ShoesCart";
import "./shoeShop.css";
import ShoeDetail from "./ShoeDetail";

class ShoesShop extends Component {
  renderCart = () => {
    if (this.props.cart.length !== 0) {
      return (
        <div className="cart-quantity absolute top-1/2 left-1/2 w-6 h-6 bg-white border-2 border-cyan-500 rounded-full">
          <div className="flex items-center justify-center w-full h-full">
            <span className="text-5 text-current">
              {this.props.cart.length}
            </span>
          </div>
        </div>
      );
    }
  };
  render() {
    return (
      <div>
        <div className="h-24 bg-gradient-to-r from-green-400 to-blue-500 hover:from-pink-500 hover:to-yellow-500 ... flex items-center justify-center text-5xl text-white yellow">
          <h1>Shoe Shop</h1>
        </div>
        <div
          className="fixed top-3 right-5 w-16 h-16 bg-gradient-to-r from-green-400 to-blue-500 hover:from-pink-500 hover:to-yellow-500 ... overlay-slash rounded-full flex items-center justify-center cursor-pointer"
          onClick={this.props.handleOpenCart}
        >
          <span className="fa-solid fa-cart-shopping text-3xl text-white"></span>
          {this.renderCart()}
        </div>
        {this.props.isCartOpened && <ShoesCart />}
        <ListShoes />
        <ShoeDetail />
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    isCartOpened: state.cartReducer.isOpened,
    cart: state.cartReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleOpenCart: () => {
      dispatch({
        type: OPEN_CART_MODAL,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShoesShop);
